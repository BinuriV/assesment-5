#include<stdio.h>

int NoOfAttendees(int price);  
int Income(int price);
int Cost(int price);
int Profit(int price);


int NoOfAttendees(int price){

	return 120-(price-15)/5*20;	
}

int Income(int price){

	return NoOfAttendees(price)*price;
}

int Cost(int price){

	return NoOfAttendees(price)*3 +500;	
}

int Profit(int price){

	return Income(price)-Cost(price);
}

int main(){
	int price=15;
	printf("No of attendees:%d\n",NoOfAttendees(price));
	printf("Profit for Ticket Prices:%d\n",Profit(price));
	
        return 0;	

}
